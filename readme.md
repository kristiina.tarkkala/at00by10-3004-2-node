# node task

Hey there, traveller!

You have found a way to my school assignment -- good for you!

## installation

1. Clone repository `git clone`
2. Build project
3. Start the application

## bugs

I only have one bug on this site, and he's here:

![alt_text](clint-patterson-yx-stDkt9Ho-unsplash.jpg);


>Photo by <a href="https://unsplash.com/@cbpsc1?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Clint Patterson</a> on <a href="https://unsplash.com/s/photos/bug?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
  

  

If you happen to find more, feel free to open an issue.