const express = require('express');
const app = express();
const port = 5100;

/**
 * Add two numbers together
 * @param {Number} a first letter of alphabet
 * @param {Number} b second letter of alphabet
 * @returns {Number} sum of a and b
 */
const add = (a, b) => {
    return a + b;
}

app.get('', (req, res) => {
    const sum = add(1,2);
    console.log(sum);
    res.send('ok');
});

app.listen(port, () => {
    console.log('server listening on localhost:'+port);
});
